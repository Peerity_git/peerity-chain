
Steem clone of HF19, and we want to make a change to:

- group features, there will be multiple groups instead of a `big` group like in Steem
- private group with crypto based protected content, only members can `see` the data
- off-chain post/comment content storage
